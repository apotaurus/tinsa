tool
class_name GrenadeLauncher
extends Weapon

export (PackedScene) var Grenade
export (int) var grenade_velocity : int
export (int) var grenade_gravity : int

onready var shooting_timer := $ShootingTimer
 
var remaining_grenades := 3 setget set_remaining_grenades


func _ready() -> void:
	recurring_action = false 

func _get_configuration_warning() -> String:
	return "The Grenade variable could not be empty" if Grenade == null else ""

func attack() -> void:
	fire()

func fire() -> void:
	var grenade : Area2D = Grenade.instance()
	grenade.transform = player.crosshair_grenade.global_transform
	grenade.velocity = grenade.transform.x * grenade_velocity
	grenade.gravity = grenade_gravity
	player.owner.add_child(grenade)
	self.remaining_grenades -= 1

func set_remaining_grenades(new_value : int) -> void:
	remaining_grenades = new_value
	if (remaining_grenades == 0):
		_delete()
