tool
class_name Gun
extends Weapon

export (PackedScene) var Bullet

onready var shooting_timer := $ShootingTimer
 
var can_shoot := true


func _ready() -> void:
	recurring_action = true 
	
func _get_configuration_warning() -> String:
	return "The Bullet variable could not be epty" if Bullet == null else ""

func attack() -> void:
	shoot()

func shoot() -> void:
	if not can_shoot:
		return
	
	var bullet : Area2D = Bullet.instance()
	player.owner.add_child(bullet)
	bullet.transform = player.gunsight.global_transform
	bullet.connect("hit", player, "_on_Player_hit")
#	self.shots_fired += 5
	
	can_shoot = false
	shooting_timer.start()

#func set_shots_fired(new_shots : int) -> void:
#	shots_fired = new_shots
#	var progress := Hud.get_node(player.player_name + "/TextureProgress")
#	progress.value = new_shots
#	if (progress.value == progress.max_value):
#		progress.value = 0
#		player.remove_child(self)
#		player.weapon = null
#		queue_free()

func _on_ShootingTimer_timeout():
	can_shoot = true

