extends PlayerState


func enter(_msg := {}) -> void:
	player.velocity = Vector2.ZERO
	player.animation_player.play("idle")
	player.animated_sprite.play("idle")

func physics_update(_delta: float) -> void:
	if not player.is_on_floor():
		state_machine.transition_to("Fall")
		return
	
	if Input.is_action_just_pressed(player.move_up):
		state_machine.transition_to("Jump")
	elif Input.is_action_pressed(player.move_left) or Input.is_action_pressed(player.move_right):
		state_machine.transition_to("Run")
	elif player.weapon:
		if player.recurring_action_weapon and Input.is_action_pressed(player.attack):
			player.weapon.attack()
		elif !player.recurring_action_weapon and Input.is_action_just_pressed(player.attack):
			player.weapon.attack()



