extends Camera2D

export var move_speed := 0.5  # camera position lerp speed
export var zoom_speed := 0.25  # camera zoom lerp speed
export var min_zoom := 1.5  # camera won't zoom closer than this
export var max_zoom := 5  # camera won't zoom farther than this
export var margin : Vector2  # include some buffer area around targets

onready var screen_size := get_viewport_rect().size

var targets := []  # Array of targets to be tracked.

func _process(_delta) -> void:
	if !targets:
		return
	
	# Keep the camera centered between the targets
	var total_positions := Vector2.ZERO
	for target in targets:
		total_positions += target.position
	total_positions /= targets.size()
	position = lerp(position, total_positions, move_speed)
	
	# Find the zoom that will contain all targets
	var rect := Rect2(position, Vector2.ONE)
	for target in targets:
		rect = rect.expand(target.position)
	rect = rect.grow_individual(margin.x, margin.y, margin.x, margin.y)
	var full_zoom
	if rect.size.x > rect.size.y * screen_size.aspect():
		full_zoom = clamp(rect.size.x / screen_size.x, min_zoom, max_zoom)
	else:
		full_zoom = clamp(rect.size.y / screen_size.y, min_zoom, max_zoom)
	zoom = lerp(zoom, Vector2.ONE * full_zoom, zoom_speed)
	
	# For debug drawing. Remove after testing.
	get_parent().draw_cam_rect(rect)


func add_target(target):
	if not target in targets:
		targets.append(target)

func remove_target(target):
	if target in targets:
		var index = targets.find(target)
		targets.remove(index)
