class_name Player 
extends KinematicBody2D

# Horizontal speed in pixels per second.
export var speed := 700.0
# Vertical acceleration in pixel per second squared.
export var gravity := 3200.0
export var jump_impulse := 1000.0
export (float, 0, 1.0) var friction = 0.1
export (float, 0, 1.0) var acceleration = 0.25

export var move_right := "move_right"
export var move_left := "move_left"
export var move_down := "move_down"
export var move_up := "move_up"
export var attack := "attack"
export (String) var player_name
export (int) var kills_count 

onready var initial_scale := scale
onready var fsm := $StateMachine
onready var animation_player := $AnimationPlayer
onready var sprite := $Sprite
onready var animated_sprite := $AnimatedSprite 
onready var gunsight := $Gunsight
onready var crosshair_grenade := $CrosshairGrenade
onready var kills_label := $Kills

var flip_h := false setget set_flip_h
var velocity = Vector2.ZERO
var pickup : Node2D = null
var weapon : Node2D = null
var recurring_action_weapon := false


func _ready():
	kills_label.text = str(kills_count)

func set_flip_h(flip: bool) -> void:
	if flip_h != flip:
		flip_h = flip
	
	if flip_h:
		scale.x = -initial_scale.x * sign(scale.y)
	else:
		scale.x = initial_scale.x * sign(scale.y)
#	if (flip != animated_sprite.flip_h):
#		animated_sprite.flip_h = flip

func die() -> void:
	var camera := owner.get_node("Camera")
	camera.remove_target(self)
	#queue_free()

func _on_VisibilityNotifier2D_screen_exited() -> void:
	die()
	queue_free()
	#get_tree().reload_current_scene()

func _on_Player_hit() -> void:
	var kills : int = kills_label.text.to_int()
	kills_label.text = str(kills+1)

func _on_pick(_weapon: Node2D) -> void:
	pass
