tool
class_name WeaponArea
extends Pickup


export (PackedScene) var Weapon 

var weapon : Node2D = null

func _ready():
	weapon = Weapon.instance()
	add_child(weapon)

func _get_configuration_warning() -> String:
	return "This scene must have a Weapon child node" if Weapon == null else ""

func initialize_weapon(player : KinematicBody2D) -> void:
	weapon.player = player
	player.pickup = weapon
	player.weapon = weapon
	player.recurring_action_weapon = weapon.recurring_action

func _on_body_entered(body):
	if body.is_in_group("players"): 
		if weapon.get_parent():
			weapon.get_parent().remove_child(weapon)
			body.add_child(weapon)
			initialize_weapon(body)
			weapon.start_timer()
		connect("picked", body, "_on_pick")
		emit_signal("picked", weapon)
		queue_free()
