extends PlayerState

func enter(_msg := {}) -> void:
	player.velocity.y = -player.jump_impulse
	player.animated_sprite.play("jump")


func physics_update(delta: float) -> void:
	var input_direction_x: float = (
		Input.get_action_strength(player.move_right)
		- Input.get_action_strength(player.move_left)
	)
	if (input_direction_x < 0):
		player.flip_h = true
	elif (input_direction_x > 0):
		player.flip_h = false
	
	player.velocity.x = player.speed * input_direction_x
	player.velocity.y += player.gravity * delta
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)

	if player.velocity.y > 0:
		state_machine.transition_to("Fall")
	elif Input.is_action_just_pressed(player.move_up):
		state_machine.transition_to("DoubleJump")

