extends PlayerState

var coyote_jump: = false

func enter(_msg := {}) -> void:
	player.animated_sprite.play("run")

func physics_update(delta: float) -> void:
	if player.is_on_floor():
		coyote_jump = true
	else:
		delay_jump()
		#state_machine.transition_to("Fall")
	
	var input_direction_x: float = (
		Input.get_action_strength(player.move_right)
		- Input.get_action_strength(player.move_left)
	)
	
	if input_direction_x != 0:
		if (input_direction_x < 0):
			player.flip_h = true
		elif (input_direction_x > 0):
			player.flip_h = false
		player.velocity.x = lerp(player.velocity.x, input_direction_x * player.speed, player.acceleration)
	else:
		player.velocity.x = lerp(player.velocity.x, 0, player.friction)
	player.velocity.y += player.gravity * delta
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	
	if is_equal_approx(input_direction_x, 0.0):
		state_machine.transition_to("Idle")
	
	if Input.is_action_just_pressed(player.move_up):
		if coyote_jump:
			state_machine.transition_to("Jump")
	elif Input.is_action_pressed(player.attack) and player.weapon:
		player.weapon.attack()


func delay_jump() -> void:
	yield(get_tree().create_timer(.05), "timeout")
	coyote_jump = false
