extends Area2D

signal hit

export var speed : int

func _physics_process(delta):
	position += transform.x * speed * delta

func _on_body_entered(body : KinematicBody2D):
	if body.is_in_group("players"): 
		emit_signal("hit")
		#body.die()
	queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
