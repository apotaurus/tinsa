extends PlayerState


func enter(_msg := {}) -> void:
	player.animated_sprite.play("fall")


func physics_update(delta: float) -> void:
	if (player.velocity.x < 0):
		player.flip_h = true
	elif (player.velocity.x > 0):
		player.flip_h = false
	
	player.velocity.y += player.gravity * delta
	player.velocity = player.move_and_slide(player.velocity, Vector2.UP)
	
	if player.is_on_floor():
		if is_equal_approx(player.velocity.x, 0.0):
			state_machine.transition_to("Idle")
		else:
			state_machine.transition_to("Run")
