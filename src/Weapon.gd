class_name Weapon
extends Node2D

export var life := 30

onready var life_timer := $LifeTimer
onready var player : KinematicBody2D = null

var progress : TextureProgress = null
var counter_timer := 0 setget set_counter_timer
var recurring_action : bool

func start_timer() -> void:
	progress = Hud.get_node(player.player_name + "/TextureProgress")
	progress.max_value = life
	life_timer.start()

func attack() -> void:
	pass

func set_counter_timer(new_value : int) -> void:
	counter_timer = new_value
	progress.value = new_value
	if (progress.value == progress.max_value):
		progress.value = 0
		_delete()

func _delete() -> void:
	player.remove_child(self)
	player.weapon = null
	queue_free()

func _on_LifeTimer_timeout():
	self.counter_timer += 1
