extends Node2D

onready var camera := $Camera
onready var tilemap := $TileMap

var cam_rect := Rect2()
var debug := false

func _ready():
	var index := 1
	for player in get_tree().get_nodes_in_group("players"):
		camera.add_target(player)
		var player_name : String = player.player_name
		Hud.get_node("Player " + str(index) + "/Name").text = player_name
		index += 1
	
	# Set camera limits
	var rect = tilemap.get_used_rect()
	camera.limit_left = rect.position.x * tilemap.cell_size.x
	camera.limit_right = rect.end.x * tilemap.cell_size.x


func draw_cam_rect(rect):
	cam_rect = rect
	update()

func _draw():
	# For debugging, draw the bounds rectangle for camera zoom
	if !debug:
		return
	draw_circle(camera.position, 10, Color(1.0, .33, .3))
	draw_rect(cam_rect, Color(1.0, .33, .3), false, 4.0)

func _input(event):
	if event.is_action_pressed("ui_focus_next"):
		debug = !debug
